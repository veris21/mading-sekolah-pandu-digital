import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as fb_storage;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await initializeDateFormatting('id_ID', null)
      .then((_) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mading Pandu Digital',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MadingPanduDigital(title: 'Mading Pandu Digital'),
    );
  }
}

class MadingPanduDigital extends StatefulWidget {
  const MadingPanduDigital({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MadingPanduDigital> createState() => _MadingPanduDigitalState();
}

class _MadingPanduDigitalState extends State<MadingPanduDigital> {
  Stream<QuerySnapshot>? _listMading;
  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future _loadData() async {
    _listMading = FirebaseFirestore.instance.collection('mading').snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset('assets/pandu.png'),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StreamBuilder<QuerySnapshot>(
              stream: _listMading,
              builder: (_, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (snapshot.hasData) {
                  if (snapshot.data!.docs.isEmpty) {
                    return Center(
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/noData.png',
                            fit: BoxFit.contain,
                          ),
                          SizedBox(height: 16),
                          Text("Data Kosong"),
                        ],
                      ),
                    );
                  }
                  return ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children: snapshot.data!.docs.map((doc) {
                      Map<String, dynamic> data =
                          doc.data()! as Map<String, dynamic>;
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 12),
                        child: GestureDetector(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) =>
                                        DetailsMading(documents: data))),
                            child: Card(
                              child: ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: NetworkImage(data['image']),
                                ),
                                title: Text(data['title']),
                                subtitle: Text(
                                  DateFormat('dd MMMM yyyy')
                                      .format(DateTime.parse(data['date'])),
                                ),
                              ),
                            )

                            //  Container(
                            //   height: MediaQuery.of(context).size.height * .3,
                            //   decoration: BoxDecoration(
                            //     border: Border.all(
                            //       color: Colors.black,
                            //       width: 1,
                            //     ),
                            //     borderRadius: BorderRadius.circular(8),
                            //   ),
                            //   child: Column(
                            //     children: <Widget>[
                            //       Container(
                            //         height:
                            //             MediaQuery.of(context).size.height * .2,
                            //         decoration: BoxDecoration(
                            //           borderRadius: BorderRadius.only(
                            //             topLeft: Radius.circular(8),
                            //             topRight: Radius.circular(8),
                            //           ),
                            //         ),
                            //         child: data['image'] == null ||
                            //                 data['image'] == ""
                            //             ? Image.asset("assets/pandu.png",
                            //                 fit: BoxFit.cover)
                            //             : Image.network(
                            //                 data['image'],
                            //                 fit: BoxFit.cover,
                            //               ),
                            //       ),
                            //       Padding(
                            //         padding: EdgeInsets.symmetric(
                            //             horizontal: 12.0, vertical: 8),
                            //         child: FittedBox(
                            //           fit: BoxFit.contain,
                            //           child: Text(
                            //             data['title'],
                            //             style: const TextStyle(
                            //               fontSize: 20,
                            //               fontWeight: FontWeight.bold,
                            //             ),
                            //           ),
                            //         ),
                            //       ),
                            //       Padding(
                            //         padding:
                            //             EdgeInsets.symmetric(horizontal: 12.0),
                            //         child: FittedBox(
                            //           fit: BoxFit.contain,
                            //           child: Text(
                            //             "Pada ${DateFormat('dd MMMM yyyy').format(DateTime.parse(data['date']))}",
                            //             style: const TextStyle(
                            //               fontSize: 12,
                            //               fontWeight: FontWeight.bold,
                            //             ),
                            //           ),
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            ),
                      );
                    }).toList(),
                  );
                } else {
                  return Center(
                    child: Image.asset(
                      'assets/noData.png',
                      fit: BoxFit.contain,
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => const AddMading()))
        },
        tooltip: 'Input Mading',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class DetailsMading extends StatelessWidget {
  final Map documents;
  const DetailsMading({Key? key, required this.documents}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(documents['title']),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              documents['image'],
              fit: BoxFit.contain,
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                documents['title'],
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "di Posting Pada : ${DateFormat('dd MMMM yyyy').format(
                  DateTime.parse(documents['date']),
                )}",
                textAlign: TextAlign.right,
              ),
            ),
            const Divider(),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12),
              child: Text(documents['description']),
            ),
          ],
        ),
      ),
    );
  }
}

class AddMading extends StatefulWidget {
  const AddMading({Key? key}) : super(key: key);
  @override
  _AddMadingState createState() => _AddMadingState();
}

class _AddMadingState extends State<AddMading> {
  final fb_storage.FirebaseStorage _storage =
      fb_storage.FirebaseStorage.instance;
  final TextEditingController judulController = TextEditingController();
  final TextEditingController deskripsiController = TextEditingController();
  String? title;
  File? image;
  String? description;
  String? imageUrl;
  final ImagePicker imagePicker = ImagePicker();

  bool? _isLoading = false;
  bool? _uploadingImage = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getImage(ImageSource source) async {
    _uploadingImage = true;
    final pickedFile = await imagePicker.pickImage(source: source);
    if (pickedFile != null) {
      fb_storage.SettableMetadata metadata = fb_storage.SettableMetadata(
        cacheControl: 'max-age=60',
        customMetadata: <String, String>{
          'image': DateTime.now().millisecondsSinceEpoch.toString(),
          'status': 'Pandu Digital 8 November 2021'
        },
      );
      await _storage
          .ref()
          .child('mading/${DateTime.now().millisecondsSinceEpoch}')
          .putFile(File(pickedFile.path), metadata)
          .then((p0) {
        p0.ref.getDownloadURL().then((value) {
          setState(() {
            imageUrl = value;
            _uploadingImage = false;
            image = File(pickedFile.path);
          });
        });
      });
    }
  }

  void _postingMadings(BuildContext context) async {
    try {
      _isLoading = true;
      final String judul = judulController.text;
      final String deskripsi = deskripsiController.text;
      final String date = DateTime.now().toString();
      print(imageUrl);
      final Map<String, dynamic> data = {
        'title': judul,
        'description': deskripsi,
        'date': date,
        'image': imageUrl,
      };
      await FirebaseFirestore.instance
          .collection('mading')
          .add(data)
          .then((value) {
        _isLoading = false;
        Navigator.pop(context);
      });
    } catch (e) {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Input Mading'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
          child: _isLoading == true
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: <Widget>[
                    image != null
                        ? Container(
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: FileImage(image!),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )
                        : Container(
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/noData.png'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: judulController,
                        onChanged: (value) => title = value,
                        decoration: const InputDecoration(
                          labelText: 'Judul',
                          hintText: 'Masukkan Judul',
                        ),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: deskripsiController,
                        onChanged: (value) => description = value,
                        decoration: const InputDecoration(
                          labelText: 'Deskripsi',
                          hintText: 'Masukkan Deskripsi',
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                      ),
                    ),
                    const Divider(),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            title: const Text('Pilih Gambar'),
                            content: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    getImage(ImageSource.camera);
                                  },
                                  child: const Text('Kamera'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    getImage(ImageSource.gallery);
                                  },
                                  child: const Text('Galeri'),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.05,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: _uploadingImage == true
                              ? CircularProgressIndicator()
                              : Text('Pilih Gambar Artikel'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Divider(),
                    GestureDetector(
                      onTap: () => _postingMadings(context),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.05,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.blue,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: const Center(
                            child: Text('Posting Mading',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
